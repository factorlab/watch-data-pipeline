# Databricks notebook source
# DBTITLE 1,Configs
bronze_path = "s3a://factorlab-databricks-bronze-data/watch_data/"
silver_path = "s3a://factorlab-databricks-silver-data/watch_data/"
silver_bad_data_path = "s3a://factorlab-databricks-silver-data/watch_bad_data/"
merge_temp_path = "s3a://factorlab-databricks-silver-data/temp/silver_merge/"
silver_checkpoint_path = "{}/_checkpoint/".format(silver_path)
code_version = "v0.1"

# COMMAND ----------

# DBTITLE 1,Refresh FactorLab Watch Tables:
# MAGIC %run /Shared/Production/factorlab-data-utils/utils/refresh_watch_fl_keys

# COMMAND ----------

updateWatchFlKey()

# COMMAND ----------

# dbutils.fs.ls(silver_path)
# dbutils.fs.rm(silver_path, True)

# COMMAND ----------

# DBTITLE 1,Imports
import time
import numpy as np
import pandas as pd
import pyspark.sql.types as pst
import pyspark.sql.functions as psf
import databricks.koalas as ks
from pyspark.sql.utils import AnalysisException

# COMMAND ----------

# DBTITLE 1,Internal variables
freq = 25 #times per second
schema_dict = {
 'activity': 'STRING',
 'altitude': 'FLOAT',
 'cadence': 'FLOAT',
 'zone_id': 'BIGINT',
 'distance': 'FLOAT',
 'flight_asc': 'INT',
 'flight_desc': 'INT',
 'heading_angle': 'FLOAT',
 'heart_rate': 'FLOAT',
 'latitude': 'FLOAT',
 'longitude': 'FLOAT',
 'pace': 'FLOAT',
 'pressure': 'FLOAT',
 'rotation_angle': 'FLOAT',
 'steps': 'INT',
 'temperature': 'FLOAT',
 'time_stamp': 'BIGINT',
 'w_quaternion': 'FLOAT',
 'watch_handedness': 'STRING',
 'watch_orientation': 'INT',
 'x_acceleration': 'FLOAT',
 'x_quaternion': 'FLOAT',
 'x_rotationRate': 'FLOAT',
 'x_user_accel': 'FLOAT',
 'y_acceleration': 'FLOAT',
 'y_quaternion': 'FLOAT',
 'y_rotationRate': 'FLOAT',
 'y_user_accel': 'FLOAT',
 'z_acceleration': 'FLOAT',
 'z_quaternion': 'FLOAT',
 'z_rotationRate': 'FLOAT',
 'z_user_accel': 'FLOAT',
 'fl_key': 'BIGINT'}

type_default_dict = {"INT":0, "BIGINT":0,"FLOAT": 0.0, "STRING":"''"}
# for the columns in key, replace (null, ' ', '') with the specified values - this step has some overlap with the casting step. I kept it consistent with the original ks code but consider reducing the overlap
preprocess_dict = {
  'activity': 'Stationary',
  'altitude': '0',
  'cadence': '0',
  'distance': '0',
  'flight_asc': '0',
  'flight_desc': '0',
  'heading_angle': '0',
  'heart_rate': '0',
  'latitude': '0',
  'longitude': '0',
  'pace': '0',
  'pressure': '0',
  'rotation_angle': '0',
  'steps': '0',
  'temperature': '0',
  'w_quaternion': '0',
  'x_acceleration': '0',
  'x_quaternion': '0',
  'x_rotationRate': '0',
  'x_user_accel': '0',
  'y_acceleration': '0',
  'y_quaternion': '0',
  'y_rotationRate': '0',
  'y_user_accel': '0',
  'z_acceleration': '0',
  'z_quaternion': '0',
  'z_rotationRate': '0',
  'z_user_accel': '0'
}

# COMMAND ----------

spark.conf.set("spark.sql.files.maxPartitionBytes", 1024*1024*512)
spark.conf.set("spark.databricks.delta.properties.defaults.enableChangeDataFeed", "true")
spark.conf.set("spark.databricks.delta.formatCheck.enabled", "false")

# COMMAND ----------

def preprocess_dataframe(spark_df):
  def preprocess_column(df, column):
    replacement = preprocess_dict[column]
    return df.withColumn(column, psf.expr(f"case when {column} is null or {column} in (0, 'NaN') or {column} rlike '^\s*$' then '{replacement}' else {column} end")) # aside from the in () syntax, you can use "rlike '<regex string>'" in spark sql for regex matching
  col_list = spark_df.columns

  if 'owner_name' in col_list:
    spark_df = spark_df.drop("owner_name")
                                   
  if 'uuid' in col_list:
    spark_df = spark_df.drop("uuid")
                                   
  if 'iPhone_Udid' in col_list:
    spark_df = spark_df.drop("iPhone_Udid")
                                   
  if 'session_id' in col_list:
    spark_df = spark_df.drop("session_id")
                                   
  # Custom data cleansing and transformation
  for col in preprocess_dict.keys():
    if col in col_list:
      spark_df = preprocess_column(spark_df, col)
    else:
      spark_df = spark_df.withColumn(col, psf.lit(preprocess_dict[column])) 
  
  if 'watch_handedness' in col_list:
    spark_df = spark_df.withColumn("watch_handedness",psf.expr("case when watch_handedness in ('0', 'L') then 'L' else 'R' end ")) # this column does not have an else branch here according to the original code
                                   
  if 'zone_id' in col_list:
    spark_df = spark_df.withColumn("zone_id", psf.expr(f"""case when (zone_id is null or zone_id in ('index', '0') or zone_id rlike '^\s*$') and (date_value < '2021-08-12') then '20236' 
      when (zone_id is null or zone_id in ('index', '0') or zone_id rlike '^\s*$') and (date_value >= '2021-08-12') then '00000' 
      else zone_id end"""))
  else:
    spark_df = spark_df.withColumn("zone_id", psf.expr(f"case when date_value < '2021-08-12' then '20236' else null end")) 
  
  if 'fl_key' in col_list:
    spark_df = spark_df.withColumn("fl_key", psf.expr(f"case when fl_key is null or fl_key in ('0', 'NaN') or fl_key rlike '^\s*$' then '10004' else fl_key end")) # assumes the float nan becomes 'NaN' if written as a string; this is how Spark internally converts NaN's
  else:
    spark_df = spark_df.withColumn("fl_key", psf.lit("10004")) 
    
  spark_df = spark_df.withColumn("time_stamp", psf.expr("left(time_stamp, 13)")) 
  return spark_df

# COMMAND ----------

# data casting with defaults
def cast_dataframe(spark_df):
  for col in schema_dict.keys():
    spark_df = spark_df.withColumn(col, psf.expr(f"""coalesce(cast({col} as {schema_dict[col]}), {type_default_dict[schema_dict[col]]})""")) 
  return spark_df

# COMMAND ----------

def transform_datetime(spark_df):
  return (spark_df
          .withColumn("timestamp_value", 
           psf.to_timestamp(psf.concat_ws(".",psf.from_unixtime(psf.substring(psf.col("time_stamp"),0,10),"yyyy-MM-dd HH:mm:ss"), psf.substring(psf.col("time_stamp"),-3,3))))
  .withColumn("time_stamp_hour", psf.hour(psf.col("timestamp_value")))
  .withColumn("time_stamp_minute", psf.minute(psf.col("timestamp_value"))))

# COMMAND ----------

def resampleDataframe(df, sample_frequency):
  sample_rate = str(int(1000 / sample_frequency)) + "ms"
  time_stamp_min = df.groupby(['zone_id','fl_key','date_value','time_stamp_hour','time_stamp_minute'])['time_stamp'].transform(min) # added zone_id in groupby
  df['start_time'] = time_stamp_min - (time_stamp_min % 60000) # to round down to the multiple of 60 seconds, as Anthony suggested
  df.loc[:,'time_delta'] = pd.to_timedelta((df.loc[:,'time_stamp'] - df.loc[:,'start_time'])*1e6)
  df.set_index('time_delta', inplace=True)
  agg_rules = { "activity": "first", 
                "altitude": "mean",
                "cadence": "mean",
#                 "zone_id": "first", # "zone_id" should be part of groupping, not the aggregation
                "distance": "mean",
                "flight_asc": "mean",
                "flight_desc": "mean",
                "heading_angle": "mean",
                "heart_rate": "mean",
                "latitude": "mean",
                "longitude": "mean",
                "pace": "mean",
                "pressure": "mean",
                "rotation_angle": "mean",
                "start_time": "min", # used min instead of first for consistency
                "steps": "mean",
                "temperature": "mean",
                "time_stamp": "min",# used min instead of first for consistency
                "timestamp_value": "min",# used min instead of first for consistency
                "w_quaternion": "mean",
                "watch_handedness": "last",
                "watch_orientation": "first",
                "x_acceleration": "mean",
                "x_quaternion": "mean",
                "x_rotationRate": "mean",
                "x_user_accel": "mean",
                "y_acceleration": "mean",
                "y_quaternion": "mean",
                "y_rotationRate": "mean",
                "y_user_accel": "mean",
                "z_acceleration": "mean",
                "z_quaternion": "mean",
                "z_rotationRate": "mean",
                "z_user_accel": "mean",
                }
  
  df_resampled = df.groupby(['zone_id', 'fl_key','date_value','time_stamp_hour','time_stamp_minute']).resample(rule=sample_rate).agg(agg_rules).reset_index() # added zone_id in groupby

  df_resampled = df_resampled.interpolate() # this will do a linear interpolation - I see bfill being passed in some examples but it doesn't seem documented. Do we want to do bfill instead of linear interpolation?
  # zone_id MUST be in the group by because otherwise it can get interpolated and contain non-sensible results
  # Not sure this reset_index is necessary.
  df_resampled = df_resampled.reset_index(drop=True)
  df_resampled['start_datetime'] = pd.to_datetime(df_resampled['start_time'], unit='ms')
  df_resampled.timestamp_value = (df_resampled.start_datetime + df_resampled.time_delta) # this used to be where time_stamp was casted to Timestamp type; I have applied this typed and rounded timestamp to timestamp_value, which is the appropriate column with Timestamp Type
  return df_resampled

# COMMAND ----------

def processAndResampleUDF(df):
  return resampleDataframe(df, freq)

return_schema = 'fl_key bigint,date_value date,time_stamp_hour bigint,time_stamp_minute bigint,time_delta bigint,activity string,altitude double,cadence double,zone_id bigint,distance double,flight_asc double,flight_desc double,heading_angle double,heart_rate double,latitude double,longitude double,pace double,pressure double,rotation_angle double,start_time double,steps int,temperature double,time_stamp bigint,timestamp_value timestamp,w_quaternion double,watch_handedness string,watch_orientation double,x_acceleration double,x_quaternion double,x_rotationRate double,x_user_accel double,y_acceleration double,y_quaternion double,y_rotationRate double,y_user_accel double,z_acceleration double,z_quaternion double,z_rotationRate double,z_user_accel double,start_datetime timestamp'

# COMMAND ----------

def get_full_batch_df(batch_df):
  batch_df.selectExpr("fl_key", "date_trunc('minute', from_unixtime(time_stamp/1000)) as _minute" , "date_value").dropDuplicates().createOrReplaceGlobalTempView("fl_minute")
  return spark.sql(f"""select /*+ BROADCAST(b) */ a.*
  from delta.`{bronze_path}` a join global_temp.fl_minute b
  on a.fl_key = b.fl_key 
    and a.date_value = b.date_value
    and date_trunc('minute', from_unixtime(a.time_stamp/1000)) = b._minute
  """) # added date_value for partition pruning: https://jaceklaskowski.github.io/mastering-spark-sql-book/new-and-noteworthy/dynamic-partition-pruning/

# COMMAND ----------

def merge_into_silver(output_df):
  output_df.write.mode("overwrite").option("overwriteSchema", "true").save(merge_temp_path) 
  spark.read.load(merge_temp_path).createOrReplaceGlobalTempView("output_df")
  try:
    spark.sql(f"""MERGE INTO delta.`{silver_path}` a
      using global_temp.output_df b
      ON a.zone_id = b.zone_id and a.fl_key = b.fl_key and a.timestamp_value = b.timestamp_value and a.date_value = b.date_value
      when matched then update set *
      when not matched then insert *
      """) 
    # note that the join key is the rounded timestamp (rounded to the nearest multiple of 60 seconds in UNIX time)
    # added date_value for partition pruning in MERGE: https://kb.databricks.com/delta/delta-merge-into.html#how-to-improve-performance-of-delta-lake-merge-into-queries-using-partition-pruning
    
  except AnalysisException: # occurs on the very first write when the silver delta table does not exist
    spark.read.load(merge_temp_path).write.format("delta").partitionBy("zone_id", "date_value").save(silver_path) 

# COMMAND ----------

def merge_bad_data_into_silver(output_df):
  output_df.write.mode("overwrite").option("overwriteSchema", "true").save(merge_temp_path) 
  spark.read.load(merge_temp_path).createOrReplaceGlobalTempView("output_df")
  try:
    spark.sql(f"""MERGE INTO delta.`{silver_bad_data_path}` a
      using global_temp.output_df b
      ON a.zone_id = b.zone_id and a.fl_key = b.fl_key and a.timestamp_value = b.timestamp_value and a.date_value = b.date_value
      when matched then update set *
      when not matched then insert *
      """) 
    # note that the join key is the rounded timestamp (rounded to the nearest multiple of 60 seconds in UNIX time)
    # added date_value for partition pruning in MERGE: https://kb.databricks.com/delta/delta-merge-into.html#how-to-improve-performance-of-delta-lake-merge-into-queries-using-partition-pruning
    
  except AnalysisException: # occurs on the very first write when the silver bad data delta table does not exist
    spark.read.load(merge_temp_path).write.format("delta").partitionBy("zone_id", "date_value").save(silver_bad_data_path) 

# COMMAND ----------

# batch_df = spark.read.format("delta").load(bronze_path).limit(1000)

#streamDF = (spark
#  .readStream
#  .format("delta")
#  .load(bronze_path)
#)

def foreachBatchFunction(batch_df, batch_id):
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.optimizeWrite.enabled=true")
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.autoCompact.enabled=true") 
  fl_key_dim_df = spark.table("watch_fl_key").withColumnRenamed("id", "fl_key").withColumnRenamed("last_updated", "watch_fl_key_last_updated").dropDuplicates(["fl_key"]) # ensure uniqueness of id
  full_batch_df = get_full_batch_df(batch_df)
  spark_df_prep = preprocess_dataframe(full_batch_df)
  spark_df_cast = cast_dataframe(spark_df_prep)
  spark_df = transform_datetime(spark_df_cast)
  spark_df_processed = (spark_df.groupby('zone_id', 'fl_key','date_value','time_stamp_hour','time_stamp_minute') # added zone_id in groupby
                        .applyInPandas(processAndResampleUDF, schema=return_schema)
                        .withColumn("time_value", psf.expr("date_format(timestamp_value, 'HH:mm:ss.SSS')")) 
                        .withColumn("processing_time", psf.expr("now()"))
                        .withColumn("code_version", psf.lit(code_version))
                        .join(fl_key_dim_df.hint("broadcast"), on=["fl_key"], how="left_outer") 
                       )
  spark_df_bad_data = spark_df_processed.filter(spark_df_processed.zone_id != spark_df_processed.zone) #filter bad data if zone_id and zone are different
  spark_df_processed = spark_df_processed.filter(spark_df_processed.zone_id == spark_df_processed.zone)
  
  merge_into_silver(spark_df_processed)
  merge_bad_data_into_silver(spark_df_bad_data) # move bad data into seperate table

# COMMAND ----------

# DBTITLE 1,Main
(spark.readStream.format("delta").load(bronze_path)
.writeStream
.option("checkpointLocation", silver_checkpoint_path)
.trigger(once=True)
.foreachBatch(foreachBatchFunction)
.start()
.awaitTermination()
)

# COMMAND ----------

display(spark.sql(f"describe history delta.`{silver_path}`"))

# COMMAND ----------

# MAGIC %sql
# MAGIC -- UPDATE Bronze data with -1 value where steps are null.
# MAGIC -- update delta.`s3a://factorlab-databricks-bronze-data/watch_data/` set steps = -1 where steps is null
# MAGIC -- total records:   1740270155
# MAGIC -- steps with null:  335703884 
# MAGIC --select count(*) from delta.`s3a://factorlab-databricks-bronze-data/watch_data/` where steps is NULL