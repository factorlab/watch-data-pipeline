# Databricks notebook source
# DBTITLE 1,Configs
raw_path = "s3://factorlab-s3-watch-data-prod/sensor/pending/v01/raw/" # https://factorlab-s3-watch-data-prod.s3.us-east-1.amazonaws.com/sensor/pending/v01/raw/
bronze_path = "s3a://factorlab-databricks-bronze-data/watch_data/"
bronze_checkpoint_path = f"{bronze_path}_checkpoint/"
code_version = "v0.1"

# COMMAND ----------

# DBTITLE 1,Imports
import time
import pyspark.sql.types as pst
import pyspark.sql.functions as psf
from pyspark.sql.functions import lit

# COMMAND ----------

# DBTITLE 1,Spark Setting
spark.conf.set('spark.sql.caseSensitive', True)
spark.conf.set('spark.databricks.delta.formatCheck.enabled', False)
spark.conf.set("spark.databricks.delta.optimizeWrite.enabled", "true")
spark.conf.set("spark.databricks.delta.autoCompact.enabled", "true")

# COMMAND ----------

# DBTITLE 1,Schema
stringTypeFields = ['activity', 'altitude', 'cadence', 'cons_key', 'distance', 'flight_asc', 'flight_desc', 'heading_angle', 'iPhone_Udid', 'heart_rate', 'latitude', 'longitude', 'owner_name', 'pace', 'pressure', 'rotation_angle', 'session_id', 'steps', 'temperature', 'time_stamp', 'uuid', 'w_quaternion', 'watch_handedness', 'watch_orientation', 'x_acceleration', 'x_quaternion', 'x_rotationRate', 'x_user_accel', 'y_acceleration', 'y_quaternion', 'y_rotationRate', 'y_user_accel', 'z_acceleration', 'z_quaternion', 'z_rotationRate', 'z_user_accel', 'fl_Key', 'fl_key']


streamSchema = pst.StructType()
for each in stringTypeFields:
  streamSchema.add(each, pst.StringType())

# streamSchema.add('processing_time', pst.TimestampType())

# COMMAND ----------

# DBTITLE 1,Pipeline
df = (spark
      .readStream
      .format("cloudFiles")
      .option("cloudFiles.format", "json")
      .option("cloudFiles.includeExistingFiles", "true")
      .option("cloudFiles.validateOptions", "true")
      .option("cloudFiles.region", "us-east-1")
      .option("cloudFiles.backfillInterval", "1 day")
      .option("cloudFiles.fetchParallelism", 100)
      .option("cloudFiles.useNotifications", "true")
      #.option("mergeSchema", True) # this option is removed for disallowing automatic schema evolution
#       .option("multiLine", "true") # this option is removed for the new JSONL formatted data source
      .schema(streamSchema)
      .load(raw_path)
     .withColumn("fl_key", psf.expr("case when coalesce(cast(fl_Key as int), 0) = 0 then coalesce(cast(fl_key as int), 10004) else cast(fl_Key as int) end")).drop("fl_Key")      
     .withColumn("fl_key", psf.expr("cast (fl_key as string)"))
     .withColumn("time_stamp", psf.expr("cast(time_stamp as string)"))
     .withColumn("processing_time", psf.expr("now()"))
     .withColumn("input_file_name", psf.input_file_name())
     .withColumn("date_value", psf.expr("to_date(from_unixtime(time_stamp/1000))"))
     .withColumn("code_version", psf.lit(code_version))
    .withColumnRenamed("cons_key", "zone_id")
 )

(df
 .writeStream
 .format("delta")
 .partitionBy("zone_id", "date_value") 
 .outputMode("append")
 .option("checkpointLocation", bronze_checkpoint_path)
 .option("path", bronze_path)
 .option("mergeSchema", True)
 .trigger(once=True)  # or set this to whatever makes sense to the data source
 .start() 
 .awaitTermination()
)

# COMMAND ----------

