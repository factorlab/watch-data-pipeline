# Databricks notebook source
import time
import pickle
import xgboost
import sklearn
import math
import numpy as np
import pandas as pd
import ruptures as rupt
import scipy.signal as sg
import pyspark.sql.types as pst
import pyspark.sql.functions as psf
import warnings
from pyspark.sql.utils import AnalysisException
warnings.filterwarnings('ignore')  

# COMMAND ----------

# DBTITLE 1,Config
# silver_path = "s3a://test-factorlab-databricks-silver-data/watch_data/pre_prod/silver/"
# gold_path = "s3a://test-factorlab-databricks-gold-data/watch_data/pre_prod/gold/"

# TODO: add widget for stage
silver_path = "s3a://factorlab-databricks-silver-data/watch_data/"
gold_path = "s3a://factorlab-databricks-gold-data/watch_data/"

# gold_path = "s3a://test-factorlab-databricks-gold-data/watch_data/temp/test/"
gold_checkpoint_path = "{}/_checkpoints/".format(gold_path)
merge_temp_path = "s3a://factorlab-databricks-gold-data/temp/gold_merge/"
code_version = "v0.1"

# COMMAND ----------

# dbutils.fs.rm(gold_path, True)

# COMMAND ----------

# MAGIC %sql 
# MAGIC SET spark.databricks.delta.formatCheck.enabled=false

# COMMAND ----------

# DBTITLE 1,Metadata calculation
def calcMetaData(df): # this has been converted to Pyspark. This step does not need pandas udf
  weight = 0.2
  threshold = 0.1
  vm_vigor_threshold = 5
  vm_rot_vigor_threshold = 2

  return (df
         .withColumn("waist", psf.expr("case when (rotation_angle > -30 and rotation_angle < 30) or (rotation_angle > 150 and rotation_angle < 210) or (rotation_angle < -150 and rotation_angle > -210) then 1 else 0 end"))
         .withColumn("default", psf.expr("case when (rotation_angle < -30 and rotation_angle > -150) or (rotation_angle > 210 and rotation_angle < 330) then 1 else 0 end"))
         .withColumn("overhead", psf.expr("case when (waist = 1 or default = 1) then 0 else 1 end"))
         .withColumn("vm", psf.expr("9.8 * sqrt(pow(x_user_accel,2) + pow(y_user_accel,2) + pow(z_user_accel,2))"))
         .withColumn("vm_rot", psf.expr("sqrt(pow(x_rotationRate,2) + pow(y_rotationRate,2) + pow(z_rotationRate,2))"))
         .withColumn("tf1", psf.expr(f"case when vm > {vm_vigor_threshold} or vm_rot > {vm_rot_vigor_threshold} then 1 else 0 end"))
         .withColumn("tf2", psf.expr(f"case when (({weight} * vm + (1 - {weight}) * vm_rot) > {threshold}) AND (vm < {vm_vigor_threshold}) AND (vm_rot < {vm_rot_vigor_threshold}) then 1 else 0 end"))
         )

calc_meta_data_return_schema = 'fl_key bigint,date_value date,time_stamp_hour bigint,time_stamp_minute bigint,time_delta bigint,activity string,altitude double,cadence double,zone_id bigint,distance double,flight_asc double,flight_desc double,heading_angle double,heart_rate double,latitude double,longitude double,pace double,pressure double,rotation_angle double,start_time double,steps int,temperature double,time_stamp bigint,timestamp_value timestamp,w_quaternion double,watch_handedness string,watch_orientation double,x_acceleration double,x_quaternion double,x_rotationRate double,x_user_accel double,y_acceleration double,y_quaternion double,y_rotationRate double,y_user_accel double,z_acceleration double,z_quaternion double,z_rotationRate double,z_user_accel double,start_datetime timestamp, time_value string, processing_time timestamp, code_version string, zone bigint, project bigint, thing bigint, iot_device bigint, created_by bigint, watch_fl_key_last_updated timestamp, two_seconds int, eight_seconds int, twenty_seconds int, overhead double, waist double, default double, vm double, vm_rot double, tf1 double, tf2 double' 

# COMMAND ----------

# DBTITLE 1,Walk Calculation
def bandpower(x, fs, fmin, fmax):
  """
  A bandpower computation tells you where most of a signals power resides within the range of frequencies (fmin, fmax)
  :param x: Time-series of measurement values
  :param fs: Sampling frequency (How often we received a measurement)
  :param fmin: frequency min
  :param fmax: frequency max
  :return:
  """

  # Goal: Find indices in f that have values > fmax and < fmin, and
  # get the sub-array with those values
  # Then use those same indices to get the matching subarray from Pxx
  # Pass the two subarrays to trapz

  try:
    f, Pxx = sg.periodogram(x, fs, axis=0)
  except Exception as e:
    print("Anthony Beaty: type(x): " + str(type(x)))
    print("Anthony Beaty: len(x): " + str(len(x)))
    print("Anthony Beaty: x: " + str(x))
    print("Anthony Beaty: fs: " + str(fs))
    raise e
    
  f_indices = np.where(np.logical_and(f > fmin, f < fmax))[0]
  f_in_range = np.take(f, f_indices)
  Pxx_in_range = np.take(Pxx, f_indices)
  Pxx_sum = sum(Pxx_in_range)

  # return sp.trapz(Pxx_in_range, f_in_range)
  return Pxx_sum

def calcPedPower(x, fs, fmin, fmax):
  pband = bandpower(x, fs, fmin, fmax)

  fmin = 0.1
  fmax = 12.5 # Absolute cap for any signals, 50 hz sampling, nyquist limit. This depends on using 50 hz sample size
  ptot = bandpower(x, fs, fmin, fmax)

  # ped_power - percentage of power in the walk band compared with overall power in the signal
  if (ptot == 0):
    ped_power = 0
  else:
    ped_power = pband / ptot #100.0 * pband / ptot

  return ped_power

def calcPedPower_orient(x):
  orientation = x.median()
  if orientation == 1:
    return 1.0
  return 0.0

def isWalking(df):
  """
  power_thresh = 60% (can change)
  Walking is defined as percent of power in walk band relative to overall power signal > power_thresh AND
    arm orientation is within walking range (mostly below the waist)

  :param df:
  :return:
  """

  # TF1 and TF2 are mild and high vigor;
  # AND 'ped' with (TF1 || TF2), per Nov 26 2019 conversation with Vivek;
  # The meaning here is that ped should be zero unless we're at mild or higher vigor

  # in python,
  # float * bool = float

  mild_or_higher_vigor = bool(df.tf1) or bool(df.tf2)
  if df.ped_power > 0.6 : #df.ped_power > 80.0 :
    return df.ped_orient * mild_or_higher_vigor

  return 0.0

def calcWalkStateUDF(df):
  fmin = 0.5
  fmax = 2.5
  sample_rate = 25
  
  # Need a groupby for transform.  By grouping on the existing index, nothing changes other than to meet the requirement.
  df['ped_power'] = df.groupby(['zone_id','fl_key','twenty_seconds'])['x_user_accel'].transform(calcPedPower, fs=sample_rate, fmin=fmin, fmax=fmax)

  # Returns a vector of ped power orientation values
  df['ped_orient'] = df.groupby(['zone_id','fl_key','twenty_seconds'])['default'].transform(calcPedPower_orient)

  # Call isWalking using df as argument to get vector of ped values (0 or 1)

  # TF1 and TF2 are mildd and high vigor;
  # AND 'ped' with (TF1 || TF2), per Nov 26 2019 conversation with Vivek
  # The meaning here is that ped should be zero unless we're at mild or higher vigor
  df['ped'] = df[['ped_power', 'ped_orient', 'tf1', 'tf2']].apply(isWalking, axis=1)

  return df

calc_walk_data_return_schema = calc_meta_data_return_schema + ', ped_power double, ped_orient int, ped double'

# COMMAND ----------

# DBTITLE 1,Pull Calculation
def calculateIsPulling(df): # NL: this has been converted to Pyspark. This step does not need pandas udf
  return (df.selectExpr("*",
    "min(rotation_angle) over (partition by fl_key, date_value, two_seconds) as rot_ang_min",
    "max(rotation_angle) over (partition by fl_key, date_value, two_seconds) as rot_ang_max",
    "min(x_rotationRate) over (partition by fl_key, date_value, two_seconds) as x_rot_min",
    "max(x_rotationRate) over (partition by fl_key, date_value, two_seconds) as x_rot_max",
    "min(y_rotationRate) over (partition by fl_key, date_value, two_seconds) as y_rot_min",
    "max(y_rotationRate) over (partition by fl_key, date_value, two_seconds) as y_rot_max",
    "min(z_rotationRate) over (partition by fl_key, date_value, two_seconds) as z_rot_min",
    "max(z_rotationRate) over (partition by fl_key, date_value, two_seconds) as z_rot_max",
    "min(x_user_accel) over (partition by fl_key, date_value, two_seconds) as x_acc_min",
    "max(x_user_accel) over (partition by fl_key, date_value, two_seconds) as x_acc_max",
    "min(y_user_accel) over (partition by fl_key, date_value, two_seconds) as y_acc_min",
    "max(y_user_accel) over (partition by fl_key, date_value, two_seconds) as y_acc_max",
    "min(z_user_accel) over (partition by fl_key, date_value, two_seconds) as z_acc_min",
    "max(z_user_accel) over (partition by fl_key, date_value, two_seconds) as z_acc_max")
    .selectExpr("*",
    "abs(rot_ang_max - rot_ang_min) as rot_ang_dff",
    "abs(x_rot_max - x_rot_min) as x_rot_dff",
    "abs(y_rot_max - y_rot_min) as y_rot_dff",
    "abs(z_rot_max - z_rot_min) as z_rot_dff",
    "abs(x_acc_max - x_acc_min) as x_acc_dff",
    "abs(y_acc_max - y_acc_min) as y_acc_dff",
    "abs(z_acc_max - z_acc_min) as z_acc_dff")
    .selectExpr("*",
    """case when rot_ang_dff > 30 and 
      ((x_rot_dff > 2) or (y_rot_dff > 2) or (z_rot_dff > 2)) and 
      ((x_acc_dff > 0.5) or (y_acc_dff > 0.5) or (z_acc_dff > 0.5)) and 
      (ped = 0) then 1 else 0 end as pull"""
  ) # NL: check this condition - the original seemed strange - perhaps some mismatching parenthesis? 
         ).drop("rot_ang_min", "rot_ang_max", "x_rot_min", "x_rot_max", "y_rot_min", "y_rot_max", "z_rot_min", "z_rot_max", "x_acc_min", "x_acc_max", "y_acc_min", "y_acc_max", "z_acc_min", "z_acc_max", "rot_ang_dff", "x_rot_dff","y_rot_dff", "z_rot_dff", "x_acc_dff", "y_acc_dff", "z_acc_dff") # NL: dropping intermediate columns as you go can reduce memory usage

calc_pull_data_return_schema = calc_walk_data_return_schema + ', pull int'

# COMMAND ----------

# DBTITLE 1,Lift Calculation
def calculateLiftChangePoints(signal):
  numlifts = 0
  tau = 3500
  sigma = 0.00046

  algo = rupt.Pelt(model="l2").fit(signal.values)
  T = len(signal)  # Number of time points

  if T < 7:
    # this has been changed from 1 to 7 by nicole@databricks.com after researching the ruptures.Pelt code base.  
    # this is because when calling predict, a `sanity_check` is done and if (n_samples < ceil(min_size / jump) * jump + min_size) or (n_samples // jump < 1), it thinks there are not enough data points to make a prediction and throws a BadSegmentationParameters error. With the default jump (5) and min_size (2) used in the algo instance, algo cannot make a predidction if T < 7
    # sanity_check at https://github.com/deepcharles/ruptures/blob/fe2b352f43635d4909642ae104d837b5a26813c7/src/ruptures/utils/utils.py
    # predict method at https://centre-borelli.github.io/ruptures-docs/code-reference/detection/pelt-reference/#ruptures.detection.pelt.Pelt.predict
    return 0

  penalty = (sigma * sigma) * math.log(T) * tau
  change_pts = algo.predict(pen=penalty)
  change_pts = change_pts[0:-1]

  return len(change_pts)

def isLifting(changePts):
  if int(changePts) >= 1:
    return 1

  return 0

def calcLiftStatesUDF(df):
  # Need a groupby for transform.  By grouping on the existing index, nothing changes other than to meet the requirement. -- this was wrong. You need to group by the columns - Nicole
  df['lift_change_points'] = df.groupby(['zone_id','fl_key','twenty_seconds'])['pressure'].transform(calculateLiftChangePoints)
  df['lift'] = df['lift_change_points'].apply(isLifting)
  
  return df

calc_lift_data_return_schema = calc_pull_data_return_schema + ', lift_change_points int, lift int'



# COMMAND ----------

# DBTITLE 1,Bend Calculation
def calculateBendChangePoints(signal, tau_value):
  # interval_index['ii'] += 1
  # ii = interval_index['ii']

  numlifts = 0
  #detection
  #options for model l1, l2, rbf
  # L2 norm metric used to determine change point (similar to a mean of the signal)

  # tau = .25
  sigma = 0.00046

  # print("signal values: %s" % signal.values)
  algo = rupt.Pelt(model="l2").fit(signal.values)
  T = len(signal)  # Number of time points

  if T < 7:
    # this has been changed from 1 to 7 by nicole@databricks.com after researching the ruptures.Pelt code base.  
    # this is because when calling predict, a `sanity_check` is done and if (n_samples < ceil(min_size / jump) * jump + min_size) or (n_samples // jump < 1), it thinks there are not enough data points to make a prediction and throws a BadSegmentationParameters error. With the default jump (5) and min_size (2) used in the algo instance, algo cannot make a predidction if T < 7
    # sanity_check at https://github.com/deepcharles/ruptures/blob/fe2b352f43635d4909642ae104d837b5a26813c7/src/ruptures/utils/utils.py
    # predict method at https://centre-borelli.github.io/ruptures-docs/code-reference/detection/pelt-reference/#ruptures.detection.pelt.Pelt.predict
    return 0

  penalty = (sigma * sigma) * math.log(T) * tau_value
  change_pts = algo.predict(pen=penalty)
  change_pts = change_pts[0:-1]

  return len(change_pts)

def isBending(changePts, rotation_angle_difference):
  if int(changePts) >= 1 and int(rotation_angle_difference) < 20:
    return 1

  return 0

def calcBendStatesUDF(df):
  # Bending and lifting/pulling are mutually exclusive so only calculate Bend when the user isn't lifting or pulling.
  if (df['lift'].iloc[0] == 0 and df['pull'].iloc[0] == 0):
    # Need a groupby for transform.  By grouping on the existing index, nothing changes other than to meet the requirement.
    df_grouped = df.groupby(['zone_id','fl_key','two_seconds'])

    # Now let's calculate the rotation angle min and max for each 2 second chunk
    df['bend_rot_ang_min'] = df_grouped['rotation_angle'].transform('min')
    df['bend_rot_ang_max'] = df_grouped['rotation_angle'].transform('max')

    # Calculate the delta between the min and max rotation angles
    df['bend_rot_ang_dff'] = abs(df['bend_rot_ang_max'] - df['bend_rot_ang_min'])

    # Now lets calculate change points for bend.
    df['bend_change_points'] = df_grouped['pressure'].transform(calculateBendChangePoints, (50))

    df['bend'] = df.apply(lambda x: isBending(x.bend_change_points, x.bend_rot_ang_dff), axis=1)
    df = df.drop(columns = ["bend_rot_ang_min", "bend_rot_ang_max", "bend_rot_ang_dff"])
  else:
    df['bend'] = 0
    df['bend_change_points'] = 0

  return df

calc_bend_data_return_schema = calc_lift_data_return_schema + ', bend_change_points int, bend int'


# COMMAND ----------

# DBTITLE 1,Work Calculation
def calcWorkByRow(row):
  # Do work calc

  # operates on ped, lift, pull, waist, overhead, tf1, tf2, short_walk)


  working = False

  is_walking = bool(row.ped)
  is_orientation_upwards_or_horizontal = bool(row.waist) or bool(row.overhead)
  is_elevated_position = bool(row.lift)
  is_vigor_mild_or_high = bool(row.tf1) or bool(row.tf2)

  #TODO implement
  is_short_walk = True # bool(short_walk)

  if (is_walking):
    if is_short_walk:
      working = True

  else:
    # == not walking ==
    if is_vigor_mild_or_high:
      working = True

    if is_orientation_upwards_or_horizontal:
      # hoping that the person is not lying down and sleeping - we would end calling that work because of this; if we are worried about this then we can take this condition out and just   rely on vigor being at least mild vigor
      working = True
    else:
      if is_elevated_position:
        working = True

  # returned as a int, as that's the code convention here
  return int(working)

def calcWork(df):
  return (df.selectExpr("*", "(cast(ped as BOOLEAN) or cast(waist as BOOLEAN) or cast(overhead as BOOLEAN) or cast(lift as BOOLEAN) or cast(tf1 as BOOLEAN) or cast(tf2 as BOOLEAN)) as work"))

calc_work_data_return_schema = calc_bend_data_return_schema + ', work int'


# COMMAND ----------

# DBTITLE 1,Data Rollup Method
def data_rollup(df):
#     Rename rotationRate to rotationrate
    df=df.rename(columns={'x_rotationRate': 'x_rotationrate','y_rotationRate':'y_rotationrate','z_rotationRate':'z_rotationrate'})
#     If wanted we can move out of function - new_column_list
    
    #new empty rolled dataframe
    rolled_df=pd.DataFrame()
    rolled_df['watch_handedness_rolled']=np.where(df['watch_handedness'].values[0]=='L',0,1)
    rolled_df['watch_orientation_rolled']=df['watch_orientation'].values[0]
   
    #finding quantiles and sum for each sensor calculation
    for column in new_column_list:      
        if column.__contains__('quantile'):
            sensor=column.split('_quantile_rolled_')[0]
            quantile=column.split('_quantile_rolled_')[1]
            rolled_df[column]=[np.quantile(df[sensor].values,int(quantile)*0.01)]
            
        elif column.__contains__('mean'):
            sensor=column.split('_rolled_')[0]
            rolled_df[column]=[np.mean(df[sensor].values)]
            
        elif column.__contains__('steps'):
            rolled_df['steps']=df['steps'].values[-1]-df['steps'].values[0]

    return rolled_df


# COMMAND ----------

# DBTITLE 1,Model Variables:
new_column_list = ['rotation_angle_quantile_rolled_42', 'heading_angle_quantile_rolled_88', 'heading_angle_quantile_rolled_49', 'heading_angle_quantile_rolled_43', 'heading_angle_quantile_rolled_54', 'rotation_angle_quantile_rolled_44', 'rotation_angle_quantile_rolled_32', 'heading_angle_quantile_rolled_94', 'rotation_angle_quantile_rolled_64', 'heading_angle_quantile_rolled_95', 'heading_angle_quantile_rolled_38', 'y_acceleration_quantile_rolled_31', 'z_user_accel_quantile_rolled_9', 'heading_angle_quantile_rolled_42', 'heading_angle_quantile_rolled_61', 'heading_angle_quantile_rolled_62', 'heading_angle_quantile_rolled_71', 'heading_angle_quantile_rolled_80', 'heading_angle_quantile_rolled_66', 'rotation_angle_quantile_rolled_38', 'x_user_accel_quantile_rolled_91', 'rotation_angle_quantile_rolled_47', 'x_acceleration_quantile_rolled_97', 'x_user_accel_quantile_rolled_6', 'x_acceleration_quantile_rolled_41', 'y_acceleration_quantile_rolled_21', 'y_user_accel_quantile_rolled_22', 'x_acceleration_quantile_rolled_62', 'heading_angle_quantile_rolled_68', 'heading_angle_quantile_rolled_75', 'y_user_accel_quantile_rolled_31', 'y_acceleration_quantile_rolled_30', 'y_user_accel_quantile_rolled_33', 'z_user_accel_quantile_rolled_6', 'heading_angle_quantile_rolled_85', 'heading_angle_quantile_rolled_86', 'heading_angle_rolled_mean', 'x_acceleration_quantile_rolled_32', 'x_rotationrate_quantile_rolled_29', 'heading_angle_quantile_rolled_84', 'rotation_angle_quantile_rolled_63', 'x_user_accel_quantile_rolled_85', 'rotation_angle_quantile_rolled_51', 'heading_angle_quantile_rolled_29', 'heading_angle_quantile_rolled_31', 'y_acceleration_quantile_rolled_3', 'x_acceleration_quantile_rolled_50', 'rotation_angle_quantile_rolled_60', 'y_acceleration_rolled_mean', 'rotation_angle_quantile_rolled_41', 'x_user_accel_quantile_rolled_95', 'x_rotationrate_quantile_rolled_3', 'heading_angle_quantile_rolled_97', 'heading_angle_quantile_rolled_30', 'x_acceleration_quantile_rolled_76', 'rotation_angle_quantile_rolled_70', 'x_acceleration_quantile_rolled_95', 'heading_angle_quantile_rolled_5', 'y_user_accel_quantile_rolled_21', 'heading_angle_quantile_rolled_27', 'rotation_angle_quantile_rolled_50', 'heading_angle_quantile_rolled_72', 'y_rotationrate_quantile_rolled_23', 'heading_angle_quantile_rolled_78', 'x_rotationrate_quantile_rolled_96', 'heading_angle_quantile_rolled_20', 'rotation_angle_quantile_rolled_65', 'y_user_accel_quantile_rolled_75', 'heading_angle_quantile_rolled_82', 'heading_angle_quantile_rolled_76', 'rotation_angle_quantile_rolled_56', 'heading_angle_quantile_rolled_77', 'x_acceleration_quantile_rolled_44', 'heading_angle_quantile_rolled_36', 'rotation_angle_quantile_rolled_84', 'z_user_accel_quantile_rolled_5', 'y_acceleration_quantile_rolled_9', 'heading_angle_quantile_rolled_64', 'heading_angle_quantile_rolled_51', 'x_acceleration_rolled_mean', 'heading_angle_quantile_rolled_8', 'x_user_accel_quantile_rolled_71', 'x_acceleration_quantile_rolled_28', 'x_acceleration_quantile_rolled_67', 'x_acceleration_quantile_rolled_58', 'heading_angle_quantile_rolled_23', 'x_acceleration_quantile_rolled_52', 'heading_angle_quantile_rolled_47', 'y_acceleration_quantile_rolled_34', 'y_rotationrate_quantile_rolled_2', 'x_acceleration_quantile_rolled_79', 'heading_angle_quantile_rolled_45', 'heading_angle_quantile_rolled_34', 'x_acceleration_quantile_rolled_46', 'rotation_angle_quantile_rolled_83', 'heading_angle_quantile_rolled_44', 'z_user_accel_quantile_rolled_96', 'y_user_accel_quantile_rolled_32', 'y_user_accel_quantile_rolled_8', 'rotation_angle_quantile_rolled_46', 'y_acceleration_quantile_rolled_46', 'heading_angle_quantile_rolled_67', 'rotation_angle_quantile_rolled_68', 'heading_angle_quantile_rolled_60', 'x_user_accel_quantile_rolled_92', 'y_acceleration_quantile_rolled_7', 'rotation_angle_quantile_rolled_37', 'z_user_accel_quantile_rolled_8', 'rotation_angle_quantile_rolled_53', 'heading_angle_quantile_rolled_99', 'x_user_accel_quantile_rolled_84', 'y_acceleration_quantile_rolled_58', 'heading_angle_quantile_rolled_35', 'y_rotationrate_quantile_rolled_88', 'x_acceleration_quantile_rolled_56', 'x_acceleration_quantile_rolled_54', 'rotation_angle_quantile_rolled_52', 'y_acceleration_quantile_rolled_83', 'x_acceleration_quantile_rolled_87', 'x_acceleration_quantile_rolled_51', 'heading_angle_quantile_rolled_59', 'x_acceleration_quantile_rolled_30', 'x_user_accel_quantile_rolled_82', 'rotation_angle_quantile_rolled_45', 'x_user_accel_quantile_rolled_87', 'y_acceleration_quantile_rolled_28', 'heading_angle_quantile_rolled_37', 'z_user_accel_quantile_rolled_31', 'z_user_accel_quantile_rolled_87', 'heading_angle_quantile_rolled_6', 'heading_angle_quantile_rolled_33', 'heading_angle_quantile_rolled_93', 'heading_angle_quantile_rolled_50', 'x_acceleration_quantile_rolled_80', 'x_rotationrate_quantile_rolled_6', 'z_user_accel_quantile_rolled_4', 'x_acceleration_quantile_rolled_57', 'heading_angle_quantile_rolled_69', 'y_rotationrate_quantile_rolled_66', 'x_acceleration_quantile_rolled_70', 'x_acceleration_quantile_rolled_90', 'z_rotationrate_quantile_rolled_3', 'heading_angle_quantile_rolled_87', 'z_rotationrate_quantile_rolled_98', 'heading_angle_quantile_rolled_25', 'heading_angle_quantile_rolled_9', 'x_acceleration_quantile_rolled_75', 'y_acceleration_quantile_rolled_73', 'x_acceleration_quantile_rolled_45', 'y_acceleration_quantile_rolled_23', 'y_user_accel_quantile_rolled_28', 'y_rotationrate_quantile_rolled_5', 'y_acceleration_quantile_rolled_91', 'heading_angle_quantile_rolled_41', 'heading_angle_quantile_rolled_91', 'y_acceleration_quantile_rolled_63', 'heading_angle_quantile_rolled_74', 'x_acceleration_quantile_rolled_92', 'x_acceleration_quantile_rolled_77', 'y_acceleration_quantile_rolled_47', 'y_user_accel_quantile_rolled_74', 'heading_angle_quantile_rolled_56', 'heading_angle_quantile_rolled_79', 'y_user_accel_quantile_rolled_43', 'heading_angle_quantile_rolled_73', 'y_acceleration_quantile_rolled_24', 'heading_angle_quantile_rolled_22', 'y_user_accel_quantile_rolled_39', 'y_acceleration_quantile_rolled_59', 'rotation_angle_quantile_rolled_7', 'z_rotationrate_quantile_rolled_93', 'z_acceleration_quantile_rolled_55', 'y_acceleration_quantile_rolled_5', 'rotation_angle_quantile_rolled_57', 'rotation_angle_quantile_rolled_87', 'x_acceleration_quantile_rolled_88', 'x_user_accel_quantile_rolled_86', 'heading_angle_quantile_rolled_3', 'y_rotationrate_quantile_rolled_82', 'x_user_accel_quantile_rolled_77', 'x_acceleration_quantile_rolled_27', 'rotation_angle_quantile_rolled_33', 'heading_angle_quantile_rolled_89', 'rotation_angle_quantile_rolled_34', 'z_acceleration_quantile_rolled_70', 'rotation_angle_quantile_rolled_49', 'y_acceleration_quantile_rolled_40', 'heading_angle_quantile_rolled_98', 'rotation_angle_quantile_rolled_69', 'heading_angle_quantile_rolled_26', 'heading_angle_quantile_rolled_46', 'heading_angle_quantile_rolled_21', 'x_acceleration_quantile_rolled_64', 'x_user_accel_quantile_rolled_99', 'z_user_accel_quantile_rolled_91', 'rotation_angle_quantile_rolled_27', 'rotation_angle_quantile_rolled_36', 'x_user_accel_quantile_rolled_67', 'x_acceleration_quantile_rolled_29', 'rotation_angle_quantile_rolled_55', 'z_acceleration_quantile_rolled_83', 'z_acceleration_quantile_rolled_99', 'rotation_angle_quantile_rolled_54', 'x_user_accel_quantile_rolled_27', 'y_user_accel_quantile_rolled_38', 'x_acceleration_quantile_rolled_86', 'x_acceleration_quantile_rolled_93', 'heading_angle_quantile_rolled_24', 'z_rotationrate_quantile_rolled_20', 'x_user_accel_quantile_rolled_94', 'y_acceleration_quantile_rolled_29', 'x_acceleration_quantile_rolled_40', 'y_acceleration_quantile_rolled_33', 'heading_angle_quantile_rolled_32', 'x_user_accel_quantile_rolled_96', 'y_rotationrate_quantile_rolled_93', 'rotation_angle_quantile_rolled_39', 'z_acceleration_quantile_rolled_29', 'y_rotationrate_quantile_rolled_9', 'z_user_accel_quantile_rolled_78', 'x_acceleration_quantile_rolled_84', 'heading_angle_quantile_rolled_90', 'y_user_accel_quantile_rolled_23', 'y_user_accel_quantile_rolled_83', 'y_acceleration_quantile_rolled_52', 'x_acceleration_quantile_rolled_69', 'rotation_angle_quantile_rolled_85', 'z_user_accel_quantile_rolled_86', 'x_acceleration_quantile_rolled_71', 'rotation_angle_quantile_rolled_40', 'rotation_angle_quantile_rolled_5', 'y_rotationrate_quantile_rolled_83', 'rotation_angle_quantile_rolled_86', 'y_user_accel_quantile_rolled_24', 'heading_angle_quantile_rolled_40', 'y_user_accel_quantile_rolled_6', 'y_rotationrate_quantile_rolled_31', 'x_acceleration_quantile_rolled_49', 'y_user_accel_quantile_rolled_26', 'rotation_angle_quantile_rolled_30', 'y_user_accel_quantile_rolled_85', 'heading_angle_quantile_rolled_53', 'heading_angle_quantile_rolled_92', 'z_rotationrate_quantile_rolled_8', 'y_user_accel_quantile_rolled_48', 'y_rotationrate_quantile_rolled_58', 'y_acceleration_quantile_rolled_97', 'x_rotationrate_quantile_rolled_4', 'heading_angle_quantile_rolled_81', 'x_acceleration_quantile_rolled_83', 'heading_angle_quantile_rolled_55', 'heading_angle_quantile_rolled_48', 'x_rotationrate_quantile_rolled_38', 'z_rotationrate_quantile_rolled_78', 'y_acceleration_quantile_rolled_89', 'x_user_accel_quantile_rolled_78', 'rotation_angle_quantile_rolled_23', 'z_rotationrate_quantile_rolled_74', 'heading_angle_quantile_rolled_58', 'heading_angle_quantile_rolled_83', 'x_acceleration_quantile_rolled_55', 'y_acceleration_quantile_rolled_53', 'x_acceleration_quantile_rolled_42', 'x_rotationrate_quantile_rolled_94', 'steps', 'y_user_accel_quantile_rolled_34', 'heading_angle_quantile_rolled_57', 'y_rotationrate_quantile_rolled_71', 'y_acceleration_quantile_rolled_8', 'y_user_accel_quantile_rolled_81', 'y_user_accel_quantile_rolled_7', 'y_acceleration_quantile_rolled_50', 'rotation_angle_quantile_rolled_71', 'x_user_accel_quantile_rolled_22', 'y_acceleration_quantile_rolled_74', 'x_rotationrate_quantile_rolled_2', 'heading_angle_quantile_rolled_63', 'x_rotationrate_quantile_rolled_5', 'y_user_accel_quantile_rolled_30', 'y_acceleration_quantile_rolled_81', 'y_acceleration_quantile_rolled_2', 'x_acceleration_quantile_rolled_65', 'x_acceleration_quantile_rolled_6', 'z_user_accel_quantile_rolled_71', 'y_acceleration_quantile_rolled_93', 'z_user_accel_quantile_rolled_34', 'x_user_accel_quantile_rolled_55', 'x_user_accel_quantile_rolled_7', 'x_acceleration_quantile_rolled_66', 'heading_angle_quantile_rolled_39', 'rotation_angle_quantile_rolled_80', 'heading_angle_quantile_rolled_4', 'z_user_accel_quantile_rolled_23', 'x_acceleration_quantile_rolled_20', 'x_acceleration_quantile_rolled_74', 'x_user_accel_quantile_rolled_37', 'x_acceleration_quantile_rolled_31', 'heading_angle_quantile_rolled_96', 'y_user_accel_quantile_rolled_20', 'x_acceleration_quantile_rolled_43', 'x_user_accel_quantile_rolled_73', 'x_user_accel_quantile_rolled_88', 'x_acceleration_quantile_rolled_78', 'z_acceleration_quantile_rolled_39', 'rotation_angle_quantile_rolled_94', 'x_user_accel_quantile_rolled_76', 'y_user_accel_quantile_rolled_90', 'heading_angle_quantile_rolled_2', 'rotation_angle_quantile_rolled_76', 'x_user_accel_quantile_rolled_51', 'rotation_angle_quantile_rolled_78', 'x_user_accel_quantile_rolled_89', 'y_acceleration_quantile_rolled_79', 'y_rotationrate_quantile_rolled_6', 'x_acceleration_quantile_rolled_7', 'y_acceleration_quantile_rolled_65', 'z_user_accel_quantile_rolled_99', 'heading_angle_quantile_rolled_65', 'y_user_accel_quantile_rolled_25', 'z_user_accel_quantile_rolled_63', 'heading_angle_quantile_rolled_70', 'x_user_accel_quantile_rolled_65', 'z_user_accel_quantile_rolled_75', 'x_acceleration_quantile_rolled_72', 'x_acceleration_quantile_rolled_33', 'y_acceleration_quantile_rolled_68', 'y_user_accel_quantile_rolled_57', 'z_user_accel_quantile_rolled_7', 'z_acceleration_quantile_rolled_45', 'z_user_accel_quantile_rolled_29', 'z_rotationrate_quantile_rolled_29', 'x_user_accel_quantile_rolled_66', 'y_rotationrate_quantile_rolled_37', 'z_user_accel_quantile_rolled_74', 'rotation_angle_quantile_rolled_59', 'rotation_angle_quantile_rolled_4', 'z_rotationrate_quantile_rolled_9', 'z_user_accel_quantile_rolled_27', 'x_acceleration_quantile_rolled_3', 'y_rotationrate_quantile_rolled_73', 'y_user_accel_quantile_rolled_92', 'z_user_accel_quantile_rolled_76', 'y_acceleration_quantile_rolled_42', 'x_acceleration_quantile_rolled_89', 'z_acceleration_quantile_rolled_61', 'y_acceleration_quantile_rolled_56', 'x_user_accel_quantile_rolled_48', 'z_user_accel_quantile_rolled_66', 'y_rotationrate_quantile_rolled_94', 'x_acceleration_quantile_rolled_81', 'z_acceleration_quantile_rolled_21', 'z_user_accel_quantile_rolled_26', 'x_user_accel_quantile_rolled_97', 'y_user_accel_quantile_rolled_93', 'x_rotationrate_quantile_rolled_76', 'y_user_accel_quantile_rolled_65', 'y_acceleration_quantile_rolled_51', 'x_acceleration_quantile_rolled_48', 'y_user_accel_quantile_rolled_4', 'z_user_accel_quantile_rolled_3', 'y_acceleration_quantile_rolled_61', 'x_user_accel_quantile_rolled_36', 'y_acceleration_quantile_rolled_77', 'y_user_accel_quantile_rolled_2', 'y_user_accel_quantile_rolled_47', 'z_rotationrate_quantile_rolled_69', 'y_acceleration_quantile_rolled_57', 'x_acceleration_quantile_rolled_82', 'z_acceleration_quantile_rolled_59', 'y_user_accel_quantile_rolled_9', 'z_acceleration_quantile_rolled_91', 'y_user_accel_quantile_rolled_40', 'x_rotationrate_quantile_rolled_80', 'rotation_angle_quantile_rolled_25', 'rotation_angle_quantile_rolled_88', 'y_user_accel_quantile_rolled_54', 'y_acceleration_quantile_rolled_69', 'z_acceleration_quantile_rolled_44', 'y_rotationrate_quantile_rolled_24', 'y_user_accel_quantile_rolled_37', 'rotation_angle_quantile_rolled_43', 'x_acceleration_quantile_rolled_4', 'y_acceleration_quantile_rolled_36', 'y_acceleration_quantile_rolled_25', 'rotation_angle_quantile_rolled_92', 'x_user_accel_quantile_rolled_62', 'x_rotationrate_quantile_rolled_92', 'z_user_accel_quantile_rolled_64', 'y_acceleration_quantile_rolled_54', 'y_user_accel_quantile_rolled_82', 'x_acceleration_quantile_rolled_38', 'y_user_accel_quantile_rolled_36', 'z_rotationrate_quantile_rolled_32', 'z_rotationrate_quantile_rolled_26', 'z_acceleration_quantile_rolled_68', 'x_user_accel_quantile_rolled_20', 'x_user_accel_quantile_rolled_79', 'y_rotationrate_quantile_rolled_20', 'z_acceleration_quantile_rolled_66', 'x_rotationrate_quantile_rolled_93', 'y_acceleration_quantile_rolled_71', 'y_user_accel_quantile_rolled_51', 'y_user_accel_quantile_rolled_89', 'rotation_angle_quantile_rolled_72', 'rotation_angle_quantile_rolled_81', 'rotation_angle_quantile_rolled_3', 'y_acceleration_quantile_rolled_70', 'y_rotationrate_quantile_rolled_38', 'y_rotationrate_quantile_rolled_60', 'x_user_accel_quantile_rolled_70', 'y_acceleration_quantile_rolled_35', 'rotation_angle_quantile_rolled_75', 'z_acceleration_quantile_rolled_98', 'x_acceleration_quantile_rolled_37', 'z_rotationrate_quantile_rolled_27', 'z_acceleration_quantile_rolled_72', 'rotation_angle_quantile_rolled_58', 'y_user_accel_quantile_rolled_64', 'x_user_accel_quantile_rolled_46', 'x_user_accel_quantile_rolled_43', 'heading_angle_quantile_rolled_28', 'x_acceleration_quantile_rolled_53', 'z_rotationrate_quantile_rolled_76', 'z_user_accel_quantile_rolled_43', 'rotation_angle_quantile_rolled_22', 'x_user_accel_quantile_rolled_47', 'z_acceleration_quantile_rolled_26', 'rotation_angle_quantile_rolled_6', 'x_user_accel_quantile_rolled_35', 'x_acceleration_quantile_rolled_73', 'z_acceleration_quantile_rolled_8', 'z_user_accel_quantile_rolled_98', 'x_user_accel_quantile_rolled_98', 'z_user_accel_quantile_rolled_39', 'y_rotationrate_quantile_rolled_22', 'y_acceleration_quantile_rolled_4', 'z_rotationrate_quantile_rolled_22', 'x_user_accel_quantile_rolled_9', 'rotation_angle_quantile_rolled_48', 'z_rotationrate_quantile_rolled_92', 'z_rotationrate_quantile_rolled_37', 'y_user_accel_quantile_rolled_5', 'z_acceleration_quantile_rolled_7', 'x_acceleration_quantile_rolled_91', 'x_rotationrate_quantile_rolled_84', 'x_acceleration_quantile_rolled_8', 'rotation_angle_quantile_rolled_99', 'z_acceleration_quantile_rolled_48', 'x_user_accel_quantile_rolled_23', 'x_user_accel_quantile_rolled_45', 'y_rotationrate_quantile_rolled_27', 'rotation_angle_quantile_rolled_31', 'rotation_angle_quantile_rolled_77', 'y_rotationrate_quantile_rolled_69', 'z_acceleration_quantile_rolled_56', 'z_acceleration_quantile_rolled_32', 'x_user_accel_quantile_rolled_21', 'z_acceleration_quantile_rolled_82', 'x_acceleration_quantile_rolled_22', 'z_acceleration_quantile_rolled_28', 'y_rotationrate_quantile_rolled_57', 'y_user_accel_quantile_rolled_60', 'x_user_accel_quantile_rolled_61', 'z_rotationrate_quantile_rolled_95', 'z_user_accel_quantile_rolled_33', 'rotation_angle_quantile_rolled_9', 'z_acceleration_quantile_rolled_22', 'z_acceleration_rolled_mean', 'y_rotationrate_quantile_rolled_77', 'z_acceleration_quantile_rolled_37', 'x_user_accel_quantile_rolled_24', 'y_user_accel_quantile_rolled_29', 'z_user_accel_quantile_rolled_20', 'rotation_angle_quantile_rolled_82', 'x_user_accel_quantile_rolled_54', 'heading_angle_quantile_rolled_52', 'x_rotationrate_quantile_rolled_99', 'y_acceleration_quantile_rolled_84', 'rotation_angle_quantile_rolled_35', 'x_user_accel_quantile_rolled_8', 'x_acceleration_quantile_rolled_39', 'z_acceleration_quantile_rolled_43', 'z_rotationrate_quantile_rolled_81', 'x_user_accel_quantile_rolled_29', 'x_acceleration_quantile_rolled_94', 'z_rotationrate_quantile_rolled_24', 'x_acceleration_quantile_rolled_26', 'y_user_accel_quantile_rolled_84', 'rotation_angle_quantile_rolled_79', 'y_rotationrate_quantile_rolled_61', 'x_user_accel_quantile_rolled_4', 'y_user_accel_quantile_rolled_52', 'x_acceleration_quantile_rolled_68', 'y_user_accel_quantile_rolled_50', 'x_acceleration_quantile_rolled_63', 'z_acceleration_quantile_rolled_96', 'y_user_accel_quantile_rolled_49', 'y_user_accel_quantile_rolled_46', 'y_acceleration_quantile_rolled_26', 'rotation_angle_quantile_rolled_89', 'x_rotationrate_quantile_rolled_25', 'rotation_angle_quantile_rolled_90', 'z_acceleration_quantile_rolled_58', 'heading_angle_quantile_rolled_7', 'y_rotationrate_quantile_rolled_51', 'z_user_accel_quantile_rolled_80', 'z_user_accel_quantile_rolled_97', 'y_rotationrate_quantile_rolled_34', 'y_user_accel_quantile_rolled_73', 'x_user_accel_quantile_rolled_74', 'y_acceleration_quantile_rolled_98', 'y_user_accel_quantile_rolled_70', 'y_user_accel_quantile_rolled_58', 'z_rotationrate_quantile_rolled_87', 'z_user_accel_quantile_rolled_94', 'z_user_accel_quantile_rolled_22', 'x_acceleration_quantile_rolled_5', 'z_user_accel_quantile_rolled_57', 'x_acceleration_quantile_rolled_34', 'x_user_accel_quantile_rolled_80', 'x_rotationrate_quantile_rolled_88', 'z_acceleration_quantile_rolled_27', 'y_user_accel_quantile_rolled_61', 'y_rotationrate_quantile_rolled_44', 'y_user_accel_quantile_rolled_99', 'rotation_angle_quantile_rolled_24', 'y_rotationrate_quantile_rolled_86', 'z_acceleration_quantile_rolled_38', 'y_acceleration_quantile_rolled_44', 'z_rotationrate_quantile_rolled_96', 'y_acceleration_quantile_rolled_96', 'x_user_accel_quantile_rolled_93', 'y_acceleration_quantile_rolled_6', 'z_acceleration_quantile_rolled_20', 'x_rotationrate_quantile_rolled_21', 'x_user_accel_quantile_rolled_64', 'z_acceleration_quantile_rolled_86', 'x_acceleration_quantile_rolled_21', 'z_acceleration_quantile_rolled_30', 'y_rotationrate_quantile_rolled_78', 'y_acceleration_quantile_rolled_49', 'x_user_accel_quantile_rolled_42', 'y_user_accel_quantile_rolled_55', 'z_user_accel_quantile_rolled_89', 'x_acceleration_quantile_rolled_61', 'z_acceleration_quantile_rolled_51', 'z_user_accel_quantile_rolled_92', 'y_rotationrate_quantile_rolled_48']

# COMMAND ----------

# DBTITLE 1,Functions for Energy Calculation
def energy_formula(df, step):
    STEP = step
    rmin = 0.003007281061669328
    rmax = 0.6632240148146354
    tmin = 1
    tmax = 8
    duration = STEP/25
    
    met_8 = np.sqrt(9.8*(df['user_magnitude'].sum()))/STEP
    met_scaled = ((met_8-rmin)/(rmax-rmin))*(tmax-tmin)+tmin
    cal_scaled = (met_scaled*3.5*90)/STEP
    cal = cal_scaled * (duration/60)
    return cal  


def energy_function(df):
    TIME_STEPS = 200
    STEP = 200
    sensor_list = ['energy']
    processed_data = pd.DataFrame(columns = sensor_list)
    df = df.sort_values(by=['time_stamp']) # can continue to use time_stamp for sorting, even if the type has changed
    length = len(df)
    i = 0
    while(length>=TIME_STEPS):
        row = []
        subset_df = df[['x_user_accel','y_user_accel','z_user_accel']][i:i+TIME_STEPS]
        subset_df['user_magnitude']=((np.square(subset_df['x_user_accel'])  + np.square(subset_df['y_user_accel'])   + np.square(subset_df['z_user_accel'])))
        calories = energy_formula(subset_df, TIME_STEPS)
        row  .append(calories)
        df_length = len(processed_data)
        processed_data.loc[df_length] = row
        i= i + STEP
        length = length -STEP
    
    if i != len(df):
        row = []
        subset_df = df[['x_user_accel','y_user_accel','z_user_accel']][i:i+TIME_STEPS]   
        subset_df['user_magnitude']=((np.square(subset_df['x_user_accel'])  + np.square(subset_df['y_user_accel'])   + np.square(subset_df['z_user_accel'])))
        calories = energy_formula(subset_df,length)
        row  .append(calories)
        df_length = len(processed_data)
        processed_data.loc[df_length] = row
        
    return processed_data
  
def calculate_energy(input_data):
    input_data = input_data.sort_values(by=['time_stamp'])
    calories = energy_function(input_data)
    #prediction_output = list(calories.energy[0])*input_data.shape[0]
    #prediction_output = np.repeat(calories.energy[0],input_data.shape[0])
    prediction_output = calories.energy[0]
    return prediction_output / input_data.shape[0]


# COMMAND ----------

# DBTITLE 1,Predict Install based on XGBoost Model
def xgboost_prediction(input_data):
    input_data = input_data.sort_values(by=['time_stamp'])
    X_test = data_rollup(input_data)
    model = pickle.load(open('/dbfs/FileStore/pickle/xgboost_model_deployment_model_28_07_21_2_0.pkl','rb')) # please use mlflow to manage model artifact: https://databricks.com/blog/2018/06/05/introducing-mlflow-an-open-source-machine-learning-platform.html
    prediction = model.predict(X_test)
    if prediction[0] == 2:
        return 0
    return prediction[0]


# COMMAND ----------

# DBTITLE 1,Install and Energy Calculations
def installEnergyPredictionUDF(pd_df):
  pd_df['install'] = xgboost_prediction(pd_df)
  pd_df['energy'] = calculate_energy(pd_df)
  
  return pd_df

calc_install_energy_data_return_schema = calc_work_data_return_schema + ', install int, energy double'

# COMMAND ----------

def get_full_batch_df(batch_df):
  batch_df.selectExpr("fl_key", "int(unix_timestamp(timestamp_value)/40) as fourty_seconds" , "date_value").dropDuplicates().createOrReplaceGlobalTempView("fl_fourty_seconds")
  return spark.sql(f"""select a.*
  from delta.`{silver_path}` a join global_temp.fl_fourty_seconds b
  on a.fl_key = b.fl_key 
    and a.date_value = b.date_value
    and int(unix_timestamp(a.timestamp_value)/40) = b.fourty_seconds
  """) 
# added date_value for partition pruning: https://jaceklaskowski.github.io/mastering-spark-sql-book/new-and-noteworthy/dynamic-partition-pruning/
# use windows of 40 seconds because 40 is the minimal multiple of 2, 8 and 20

# COMMAND ----------

def merge_into_gold(output_df):
  output_df.write.mode("overwrite").option("overwriteSchema", "true").save(merge_temp_path) 
  spark.read.load(merge_temp_path).createOrReplaceGlobalTempView("output_df")
  try:
    spark.sql(f"""MERGE INTO delta.`{gold_path}` a
      using global_temp.output_df b
      ON a.fl_key = b.fl_key and a.timestamp_value = b.timestamp_value and a.date_value = b.date_value
      when matched then update set *
      when not matched then insert *
      """) # added date_value for partition pruning in MERGE: https://kb.databricks.com/delta/delta-merge-into.html#how-to-improve-performance-of-delta-lake-merge-into-queries-using-partition-pruning
  except AnalysisException: # occurs on the very first write when the silver delta table does not exist
    spark.read.load(merge_temp_path).write.format("delta").partitionBy("zone_id", "date_value").save(gold_path) 

# COMMAND ----------

def foreachBatchFunction(batch_df, batchId):
  # NL: foreachBatch needs this special syntax for setting spark configs
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.optimizeWrite.enabled=true")
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.autoCompact.enabled=true") # turned on because of the likelihood of small volume, frequent microbatches 
  batch_df._jdf.sparkSession().sql("set spark.databricks.delta.formatCheck.enabled=false")
  full_batch_df = get_full_batch_df(batch_df)
  spark_df = (full_batch_df.withColumn("two_seconds", psf.expr("int(unix_timestamp(timestamp_value) / 2)"))
                     .withColumn("eight_seconds", psf.expr("int(unix_timestamp(timestamp_value) / 8)"))
                     .withColumn("twenty_seconds", psf.expr("int(unix_timestamp(timestamp_value) / 20)"))
             )
  spark_df_calc_meta_data = calcMetaData(spark_df)
  spark_df_walk_data = spark_df_calc_meta_data.groupby('zone_id','fl_key','twenty_seconds').applyInPandas(calcWalkStateUDF, schema=calc_walk_data_return_schema)
  spark_df_pull_data = calculateIsPulling(spark_df_walk_data)
  spark_df_lift_data = spark_df_pull_data.groupby('zone_id','fl_key','twenty_seconds').applyInPandas(calcLiftStatesUDF, schema=calc_lift_data_return_schema)
  spark_df_bend_data = spark_df_lift_data.groupby('zone_id','fl_key','two_seconds').applyInPandas(calcBendStatesUDF, schema=calc_bend_data_return_schema)
  spark_df_work_data = calcWork(spark_df_bend_data)
  spark_df_install_energy_data = (spark_df_work_data.groupby('zone_id','fl_key','eight_seconds').applyInPandas(installEnergyPredictionUDF, schema=calc_install_energy_data_return_schema)
                                 .withColumn("processing_time", psf.expr("now()"))
                                 .withColumn("code_version", psf.lit(code_version))
                                 )
  # TODO: it's possible to merge all the applyInPandas call with a little bit more code change; it will lead to performance improvement;
#   Please come up with a function that takes 40 second trunks of data and and includes all the transformations that needed to be performed in Pandas UDF; lower levels of interval groupings (2 sec, 8 sec, 20 sec) can be done in Pandas using the appropriate columns as all columns are included in the input dataframe to the Pandas function
  merge_into_gold(spark_df_install_energy_data)

# COMMAND ----------

# DBTITLE 1,Main
(spark.readStream.format("delta") 
  .option("readChangeFeed", "true") 
  .option("startingVersion", 0) 
  .load(silver_path)
  .filter("_change_type in ('insert', 'update_postimage')")
#   .limit(1000)
  .writeStream
  .option("checkpointLocation", gold_checkpoint_path)
  .trigger(once=True)
  .foreachBatch(foreachBatchFunction)
  .start()   
  .awaitTermination()
) 

# COMMAND ----------

