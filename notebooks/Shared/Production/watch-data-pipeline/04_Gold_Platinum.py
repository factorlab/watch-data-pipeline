# Databricks notebook source
import time
import numpy as np
import pandas as pd
import pyspark.sql.types as pst
import pyspark.sql.functions as psf
from pyspark.sql.utils import AnalysisException

# COMMAND ----------

gold_path = "s3a://factorlab-databricks-gold-data/watch_data/"
platinum_path = "s3a://factorlab-databricks-platinum-data/watch_data/"
code_version = "v0.1"
refresh_window = '30 days' # can be any interval string, like '2 days', '2 years'
spark.conf.set("spark.sql.files.maxPartitionBytes", 1024*1024*512)
spark.sql("set spark.databricks.delta.optimizeWrite.enabled=true") 

# COMMAND ----------

# dbutils.fs.rm(platinum_path, True)

# COMMAND ----------

current_date_value = spark.read.load(gold_path).selectExpr("max(date_value)").first()[0]
print(f"The latest date in the gold layer is {current_date_value}")

# COMMAND ----------

for interval in ["summary_two_minutes/", "summary_one_hour/", "summary_one_day/"]: #TODO: Add 'summary_' before intervals
  try:
    display(spark.sql(f"""delete from delta.`{platinum_path}{interval}` where date_value >=  to_date('{current_date_value}' - interval {refresh_window})
    """)
    )
    print(f"Deleted dates to be refreshed for delta.`{platinum_path}{interval}` where date_value >=  to_date('{current_date_value}' - interval {refresh_window})")
    filter_string = f"date_value >=  to_date('{current_date_value}' - interval {refresh_window})"
  except AnalysisException: # first execution
    print(f"First time executing the Gold-Platinum pipeline")
    filter_string = "1 = 1"

# COMMAND ----------

agg_list = ["mean(overhead) as overhead_pct"
, "mean(waist) as waist_pct"
, "mean(default) as default_pct"
, "mean(tf1) as high_vigor_pct"
, "mean(tf2) as mild_vigor_pct"
, "1 - mean(tf1) - mean(tf2) as low_vigor_pct"
, "mean(ped) as walk_pct"
, "mean(work) as work_pct"
, "mean(pull) as pull_pct"
, "mean(pull) as temperature"
, "mean(install) as install_prediction"
, "mean(pace) as pace"
, "mean(cadence) as cadence"
, "abs(max(steps) - min(steps)) as steps"
, "sum(lift) as lift_count"
, "sum(energy) as energy_count"
, "sum(bend) as bend_count"
, "min(timestamp_value) as timestamp_value"
, "min(time_value) as time_value"
, "min(time_stamp) as time_stamp"
]
agg_expr_list = [psf.expr(agg) for agg in agg_list]

# COMMAND ----------

# MAGIC %md ## Main

# COMMAND ----------

(spark.read.format("delta")
 .load(gold_path)
 .filter(filter_string)
#  .limit(1000) # limit calls like this can be used to test pipeline with small amount of data before running full loads
 .withColumn("two_minutes", psf.expr("int(unix_timestamp(timestamp_value) / 120)"))
 .groupby('zone','project','thing','iot_device','created_by', 'zone_id','fl_key','date_value','two_minutes')
 .agg(*agg_expr_list)
 .write.format("delta").mode("append").partitionBy("zone_id", "date_value").option("mergeSchema", True).save(platinum_path + "summary_two_minutes/") 
)


# COMMAND ----------

(spark.read.format("delta")
 .load(gold_path)
 .filter(filter_string)
#  .limit(1000)
 .withColumn("one_hour", psf.expr("int(unix_timestamp(timestamp_value) / 3600)"))
 .groupby('zone','project','thing','iot_device','created_by','zone_id','fl_key','date_value','one_hour')
 .agg(*agg_expr_list)
 .write.format("delta").mode("append").partitionBy("zone_id", "date_value").option("mergeSchema", True).save(platinum_path + "summary_one_hour/") 
)

# COMMAND ----------

(spark.read.format("delta")
 .load(gold_path)
 .filter(filter_string)
 #  .limit(1000)
 .groupby('zone','project','thing','iot_device','created_by', 'zone_id','fl_key','date_value')
 .agg(*agg_expr_list)
 .write.format("delta").mode("append").partitionBy("zone_id", "date_value").option("mergeSchema", True).save(platinum_path + "summary_one_day/") 
)