# Databricks notebook source
# DBTITLE 1,Config
raw_hist_path = "s3a://factorlab-databricks-dynamodb-data/AWSDynamoDB/01628290395836-6b1d6c20/data/"
bronze_path = "s3a://factorlab-databricks-bronze-data/watch_data/"
bronze_checkpoint_path = f"{bronze_path}_checkpoint_hist/"
code_version = "v0.1"

# COMMAND ----------

import time
import pyspark.sql.types as pst
import pyspark.sql.functions as psf

# COMMAND ----------

spark.conf.set('spark.sql.caseSensitive', True)
spark.conf.set("spark.sql.files.maxPartitionBytes", 1024*1024*512)
spark.conf.set('spark.databricks.delta.formatCheck.enabled', False)
spark.conf.set("spark.databricks.delta.optimizeWrite.enabled", "true") # this config is a must - otherwise the output file sizes are a few hundred kb's and you had a bad case of the "small file problem" 

# COMMAND ----------

streamSchema = spark.read.json(raw_hist_path).schema
print(streamSchema)

# COMMAND ----------

def extractValueFromStruct(schema, prefix=None):
  for field in schema.fields:
    if prefix is None:
      colName = field.name
      dataName = field.name
    else:
      colName = prefix
      dataName = prefix + "." + field.name

    if isinstance(field.dataType,pst.StructType):
      yield from extractValueFromStruct(field.dataType, colName)
    else:
      yield psf.col(dataName).alias(colName)

extract_udf = udf(extractValueFromStruct, pst.StringType())

streamDF = (spark
  .readStream
  .format("json")
  .schema(streamSchema)
  .load(raw_hist_path)
)

streamDF = streamDF.select(psf.explode(psf.array('Item')).alias('tmp')).select('tmp.*')
streamDF = (streamDF.select(list(extractValueFromStruct(streamDF.schema)))
            .withColumn("fl_key", psf.expr("case when coalesce(cast(fl_Key as int), 0) = 0 then coalesce(cast(fl_key as int), 10004) else cast(fl_Key as int) end")).drop("fl_Key")    
            .withColumn("fl_key", psf.expr("cast (fl_key as string)"))
           .withColumn("time_stamp", psf.expr("cast(time_stamp as string)"))
           .withColumn("processing_time", psf.expr("now()"))
           .withColumn("input_file_name", psf.input_file_name())
           .withColumn("date_value", psf.expr("to_date(from_unixtime(time_stamp/1000))"))
           .withColumn("code_version", psf.lit(code_version))
           .withColumnRenamed("cons_key", "zone_id")
           )

(streamDF
 .writeStream
 .format("delta")
 .partitionBy("zone_id", "date_value")
 .outputMode("append")
 .option("checkpointLocation", bronze_checkpoint_path)
 .option("path", bronze_path)
 .trigger(once=True)
 .start()
 .awaitTermination()
)


# COMMAND ----------

